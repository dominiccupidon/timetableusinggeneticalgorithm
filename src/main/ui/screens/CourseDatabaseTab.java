package ui.screens;

import model.CourseTableModel;
import ui.TimetableApp;

import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.table.TableRowSorter;
import java.awt.*;
import java.util.regex.PatternSyntaxException;

// Responsible for graphically handling the course database
public class CourseDatabaseTab extends JPanel {
    private JTable courseTable;

    private JButton addCourse;
    private JButton deleteCourse;
    private JButton editCourse;
    private JButton finish;

    private JFormattedTextField searchField;
    private JLabel header;
    private JLabel searchLabel;

    private JScrollPane scrollPanel;
    private JPanel buttonPanel;
    private JPanel searchPanel;
    private JFrame ui;

    private TableRowSorter<CourseTableModel> rowSorter;
    private CourseTableModel tableModel;

    public CourseDatabaseTab(CourseTableModel model) {
        super();
        tableModel = model;
        initGraphics();
    }

    private void initGraphics() {
        initTable();
        initScrollPanel();
        initLabel();
        initSearchPanel();
        initButtonPanel();
        initMainPanel();
        super.setVisible(true);
    }

    private void initLabel() {
        header = new JLabel("Course Database");
        searchLabel = new JLabel("Search");
        header.setFont(header.getFont().deriveFont(24f));
        searchLabel.setLabelFor(searchField);
    }

    //TODO: Centre Align Header
    private void initMainPanel() {
        super.setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));
        super.setBorder(BorderFactory.createEmptyBorder(30, 50, 50, 50));
        super.add(header);
        super.add(Box.createRigidArea(new Dimension(0, 10)));
        super.add(scrollPanel);
        super.add(Box.createRigidArea(new Dimension(0, 25)));
        super.add(searchPanel);
        super.add(Box.createRigidArea(new Dimension(0, 25)));
        super.add(buttonPanel);
        super.setVisible(true);
    }

    private void initScrollPanel() {
        scrollPanel = new JScrollPane(courseTable,
                JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
                JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
        scrollPanel.setVisible(true);
    }

    private void initSearchPanel() {
        initTextField();
        searchPanel = new JPanel();
        searchPanel.setLayout(new BoxLayout(searchPanel, BoxLayout.LINE_AXIS));
        searchPanel.add(searchLabel);
        searchPanel.add(Box.createHorizontalGlue());
        searchPanel.add(Box.createRigidArea(new Dimension(10, 0)));
        searchPanel.add(searchField);
        searchPanel.setVisible(true);
    }

    private void initButtonPanel() {
        buttonPanel = new JPanel();
        buttonPanel.setLayout(new BoxLayout(buttonPanel, BoxLayout.LINE_AXIS));
        initEditCourseButton();
        initAddCourseButton();
        initDeleteCourseButton();
        initFinishButton();
        buttonPanel.add(Box.createHorizontalGlue());
        buttonPanel.add(editCourse);
        buttonPanel.add(Box.createRigidArea(new Dimension(50, 0)));
        buttonPanel.add(addCourse);
        buttonPanel.add(Box.createRigidArea(new Dimension(50, 0)));
        buttonPanel.add(deleteCourse);
        buttonPanel.add(Box.createRigidArea(new Dimension(50, 0)));
        buttonPanel.add(finish);
        buttonPanel.setVisible(true);
    }

    private void initEditCourseButton() {
        editCourse = new JButton("Edit Course");
        editCourse.addActionListener(e -> setEditable(true, false));
    }

    private void initAddCourseButton() {
        addCourse = new JButton("Add Course");
        addCourse.addActionListener(e -> {
            setEditable(true, false);
            ui = (JFrame) SwingUtilities.getAncestorOfClass(JFrame.class, this);
            ui.setSize(TimetableApp.WIDTH, TimetableApp.HEIGHT + 250);
            ui.setContentPane(new DataCollectionScreen(tableModel));
            ui.setVisible(true);
        });
    }

    private void initDeleteCourseButton() {
        deleteCourse = new JButton("Delete Course");
        deleteCourse.addActionListener(e -> {
            setEditable(true, false);
            if (courseTable.getSelectedRow() != -1) {
                tableModel.removeRow(courseTable.getSelectedRow());
            }
        });
    }

    private void initFinishButton() {
        finish = new JButton("Save Changes");
        finish.setEnabled(false);
        finish.addActionListener(e -> setEditable(false, true));
    }

    private void initTextField() {
        searchField = new JFormattedTextField();
        searchField.getDocument().addDocumentListener(new DocumentListener() {
            @Override
            public void insertUpdate(DocumentEvent e) {
                filterBy(searchField);
            }

            @Override
            public void removeUpdate(DocumentEvent e) {
                filterBy(searchField);
            }

            @Override
            public void changedUpdate(DocumentEvent e) {
                filterBy(searchField);
            }
        });
    }

    private void filterBy(JFormattedTextField textField) {
        RowFilter<CourseTableModel, Object> filter = null;
        try {
            filter = RowFilter.regexFilter(textField.getText());
        } catch (PatternSyntaxException p) {
            p.getMessage();
        }
        rowSorter.setRowFilter(filter);
    }

    private void initTable() {
        courseTable = new JTable(tableModel);
        courseTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        rowSorter = new TableRowSorter<>(tableModel);
        courseTable.setRowSorter(rowSorter);
        courseTable.setFillsViewportHeight(true);
        courseTable.setVisible(true);
    }

    private void setEditable(boolean b, boolean b2) {
        finish.setEnabled(b);
        editCourse.setEnabled(b2);
        addCourse.setEnabled(b2);
        deleteCourse.setEnabled(b2);
        tableModel.setCellEditable(b);
    }

    public CourseTableModel getTableModel() {
        return tableModel;
    }
}
