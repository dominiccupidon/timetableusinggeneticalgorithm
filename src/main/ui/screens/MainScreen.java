package ui.screens;

import model.CourseDatabase;
import model.CourseTableModel;

import javax.swing.*;

public class MainScreen extends JTabbedPane {

    private CourseTableModel tableModel;
    private CourseDatabaseTab courseDatabaseTab;
    private TimeTableTab timeTableTab;

    public MainScreen(CourseDatabase database) {
        super();
        tableModel = new CourseTableModel(database);
        initGraphics();
    }

    private void initGraphics() {
        courseDatabaseTab = new CourseDatabaseTab(tableModel);
        timeTableTab = new TimeTableTab();
        super.addTab("Course Data", null, courseDatabaseTab, "Course information");
        super.addTab("Timetable", null, timeTableTab, "Create Timetable");
        super.setVisible(true);
    }

    public CourseTableModel getTableModel() {
        return courseDatabaseTab.getTableModel();
    }
}
