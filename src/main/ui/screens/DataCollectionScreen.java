package ui.screens;

import model.Course;
import model.CourseDatabase;
import model.CourseTableModel;
import ui.TimetableApp;
import validation.DuplicateException;
import validation.OutOfRangeException;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.swing.*;
import java.awt.*;
import java.beans.PropertyChangeEvent;
import java.io.File;

import static javax.swing.JOptionPane.showMessageDialog;

//TODO: Add specifications and necessary comments
//      Add title and prompt that tells users to press "Enter" key to enable the Add Course Button
//      Fix OutOfRangeException Dialog to take into account number of sessions
public class DataCollectionScreen extends JPanel {
    private static final String ERROR_SOUND = "./data/ErrorSound.wav";

    private JLabel promptCourseId;
    private JLabel promptCourseName;
    private JLabel promptRoomId;
    private JLabel promptRoomName;
    private JLabel promptTeacherId;
    private JLabel promptTeacherName;
    private JLabel promptNumOfSessions;

    private JButton addCourse;
    private JButton finish;

    private JFormattedTextField enterCourseId;
    private JFormattedTextField enterCourseName;
    private JFormattedTextField enterRoomId;
    private JFormattedTextField enterRoomName;
    private JFormattedTextField enterTeacherId;
    private JFormattedTextField enterTeacherName;
    private JFormattedTextField enterNumOfSessions;

    private JPanel dataCollectionPanel;
    private JPanel buttonPanel;

    private JFrame ui;

    private int courseId;
    private int roomId;
    private int teacherId;
    private int sessions;
    private String courseName;
    private String roomName;
    private String teacherName;
    private Course course;
    private Clip clip;
    private AudioInputStream audioInputStream;
    private CourseDatabase database;

    public DataCollectionScreen() {
        super();
        database = new CourseDatabase();
        initSound();
        initGraphics();
    }

    public DataCollectionScreen(CourseTableModel model) {
        super();
        initSound();
        database = new CourseDatabase();
        try {
            database.updateDatabaseWithModel(model);
        } catch (OutOfRangeException | DuplicateException e) {
            playErrorSound();
            showMessageDialog(dataCollectionPanel,
                    e.getMessage(),
                    "Invalid ID ", JOptionPane.ERROR_MESSAGE);
        }
        initGraphics();
    }

    private void initSound() {
        try {
            audioInputStream = AudioSystem.getAudioInputStream(new File(ERROR_SOUND).getAbsoluteFile());
            clip = AudioSystem.getClip();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void playErrorSound() {
        try {
            clip.open(audioInputStream);
        } catch (Exception e) {
            e.printStackTrace();
        }
        clip.start();
    }

    private void initGraphics() {
        initLabels();
        initTextField();
        initButtonPanel();
        initDataCollectionPanel();
        super.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
        super.setBorder(BorderFactory.createEmptyBorder(30, 50, 50, 50));
        super.add(dataCollectionPanel, LEFT_ALIGNMENT);
        super.add(Box.createRigidArea(new Dimension(0, 20)));
        super.add(buttonPanel, RIGHT_ALIGNMENT);
        super.setVisible(true);
    }

    private void initLabels() {
        promptCourseId = new JLabel("Please enter the Course ID");
        promptCourseId.setVisible(true);
        promptCourseName = new JLabel("Please enter the Course Name");
        promptCourseName.setVisible(true);
        promptRoomId = new JLabel("Please enter the Room ID");
        promptRoomId.setVisible(true);
        promptRoomName = new JLabel("Please enter the Room Name");
        promptRoomName.setVisible(true);
        promptTeacherId = new JLabel("Please enter the Teacher ID");
        promptTeacherId.setVisible(true);
        promptTeacherName = new JLabel("Please enter the Teacher's Name");
        promptTeacherName.setVisible(true);
        promptNumOfSessions = new JLabel("Please enter the Number of Sessions Per Week");
        promptNumOfSessions.setVisible(true);
    }

    private void resetTextFieldValues() {
        courseId = 0;
        roomId = 0;
        courseName = "";
        roomName = "";
        teacherId = 0;
        teacherName = "";
        sessions = 0;
    }

    private boolean isDataFullyEntered() {
        return (courseId != 0) && !(courseName.equals("")) && (roomId != 0) && !(roomName.equals(""))
                && (teacherId != 0) && !(teacherName.equals("")) && (sessions != 0);
    }

    private void initTextField() {
        resetTextFieldValues();
        initTextFieldPart1();
        initTextFieldPart2();
    }

    private void initTextFieldPart1() {
        enterCourseId = new JFormattedTextField();
        enterCourseId.setMaximumSize(new Dimension(300, 0));
        enterCourseId.setValue((courseId));
        enterCourseId.addPropertyChangeListener("value", this::propertyChange);
        enterCourseId.setVisible(true);
        enterCourseName = new JFormattedTextField();
        enterCourseName.setMaximumSize(new Dimension(300, 0));
        enterCourseName.setValue(courseName);
        enterCourseName.addPropertyChangeListener("value", this::propertyChange);
        enterCourseName.setVisible(true);
        enterRoomId = new JFormattedTextField();
        enterRoomId.setMaximumSize(new Dimension(300, 0));
        enterRoomId.setValue(roomId);
        enterRoomId.addPropertyChangeListener("value", this::propertyChange);
        enterRoomId.setVisible(true);
        enterRoomName = new JFormattedTextField();
        enterRoomName.setMaximumSize(new Dimension(300, 0));
        enterRoomName.setValue(roomName);
        enterRoomName.addPropertyChangeListener("value", this::propertyChange);
        enterRoomName.setVisible(true);
    }

    private void initTextFieldPart2() {
        enterTeacherId = new JFormattedTextField();
        enterTeacherId.setMaximumSize(new Dimension(300, 0));
        enterTeacherId.setValue((teacherId));
        enterTeacherId.addPropertyChangeListener("value", this::propertyChange);
        enterTeacherId.setVisible(true);
        enterTeacherName = new JFormattedTextField();
        enterTeacherName.setMaximumSize(new Dimension(300, 0));
        enterTeacherName.setValue(teacherName);
        enterTeacherName.addPropertyChangeListener("value", this::propertyChange);
        enterTeacherName.setVisible(true);
        enterNumOfSessions = new JFormattedTextField();
        enterNumOfSessions.setMaximumSize(new Dimension(300, 0));
        enterNumOfSessions.setValue(sessions);
        enterNumOfSessions.addPropertyChangeListener("value", this::propertyChange);
        enterNumOfSessions.setVisible(true);
    }

    private void propertyChange(PropertyChangeEvent e) {
        Object source = e.getSource();
        determineTextField(source);
        if (isDataFullyEntered()) {
            addCourse.setEnabled(true);
        }
    }

    private void determineTextField(Object source) {
        if (source == enterCourseId) {
            courseId = (Integer) enterCourseId.getValue();
        }
        if (source == enterCourseName) {
            courseName = (String) enterCourseName.getValue();
        }
        if (source ==  enterRoomId) {
            roomId = (Integer) enterRoomId.getValue();
        }
        if (source == enterRoomName) {
            roomName = (String) enterRoomName.getValue();
        }
        if (source == enterTeacherId) {
            teacherId = (Integer) enterTeacherId.getValue();
        }
        if (source == enterTeacherName) {
            teacherName = (String) enterTeacherName.getValue();
        }
        if (source == enterNumOfSessions) {
            sessions = (Integer) enterNumOfSessions.getValue();
        }

    }

    private void initButton() {
        initAddCourseButton();
        initFinishButton();
    }

    private void initAddCourseButton() {
        addCourse = new JButton("Add Course");
        addCourse.setEnabled(false);
        addCourse.setVisible(true);
        addCourse.addActionListener(e -> {
            try {
                course = new Course(courseId, courseName, roomId, roomName, teacherId, teacherName, sessions);
                database.addCourse(course);
                finish.setEnabled(true);
            } catch (OutOfRangeException | DuplicateException o) {
                playErrorSound();
                showMessageDialog(dataCollectionPanel,
                        o.getMessage(),
                        "Invalid ID ", JOptionPane.ERROR_MESSAGE);
                addCourse.setEnabled(false);
            }
        });
    }

    //TODO: Ask if making mainScreen in TimetableApp class static is appropriate
    private void initFinishButton() {
        finish = new JButton("Create Database");
        finish.setEnabled(false);
        finish.setVisible(true);
        finish.addActionListener(e -> {
            ui = (JFrame) SwingUtilities.getAncestorOfClass(JFrame.class, this);
            ui.setSize(TimetableApp.WIDTH, TimetableApp.HEIGHT);
            TimetableApp.mainScreen = new MainScreen(database);
            ui.setContentPane(TimetableApp.mainScreen);
            ui.setVisible(true);
        });
    }

    private void initButtonPanel() {
        buttonPanel = new JPanel();
        initButton();
        buttonPanel.setLayout(new BoxLayout(buttonPanel, BoxLayout.LINE_AXIS));
        buttonPanel.add(Box.createHorizontalGlue());
        buttonPanel.add(addCourse);
        buttonPanel.add(Box.createRigidArea(new Dimension(50, 0)));
        buttonPanel.add(finish);
        buttonPanel.setVisible(true);
    }

    private void initDataCollectionPanel() {
        dataCollectionPanel = new JPanel();
        dataCollectionPanel.setLayout(new BoxLayout(dataCollectionPanel, BoxLayout.PAGE_AXIS));
        initDataCollectionPanelPart1();
        initDataCollectionPanelPart2();
    }

    private void initDataCollectionPanelPart1() {
        dataCollectionPanel.add(promptCourseId, LEFT_ALIGNMENT);
        dataCollectionPanel.add(Box.createRigidArea(new Dimension(0,5)));
        dataCollectionPanel.add(enterCourseId);
        dataCollectionPanel.add(Box.createRigidArea(new Dimension(0,20)));
        dataCollectionPanel.add(Box.createVerticalGlue());
        dataCollectionPanel.add(promptCourseName, LEFT_ALIGNMENT);
        dataCollectionPanel.add(Box.createRigidArea(new Dimension(0,5)));
        dataCollectionPanel.add(enterCourseName);
        dataCollectionPanel.add(Box.createRigidArea(new Dimension(0,20)));
        dataCollectionPanel.add(Box.createVerticalGlue());
        dataCollectionPanel.add(promptRoomId, LEFT_ALIGNMENT);
        dataCollectionPanel.add(Box.createRigidArea(new Dimension(0,5)));
        dataCollectionPanel.add(enterRoomId);
        dataCollectionPanel.add(Box.createRigidArea(new Dimension(0,20)));
        dataCollectionPanel.add(Box.createVerticalGlue());
        dataCollectionPanel.add(promptRoomName, LEFT_ALIGNMENT);
        dataCollectionPanel.add(Box.createRigidArea(new Dimension(0,5)));
        dataCollectionPanel.add(enterRoomName);
        dataCollectionPanel.add(Box.createRigidArea(new Dimension(0,20)));
        dataCollectionPanel.setVisible(true);
    }

    private void initDataCollectionPanelPart2() {
        dataCollectionPanel.add(promptTeacherId, LEFT_ALIGNMENT);
        dataCollectionPanel.add(Box.createRigidArea(new Dimension(0,5)));
        dataCollectionPanel.add(enterTeacherId);
        dataCollectionPanel.add(Box.createRigidArea(new Dimension(0,20)));
        dataCollectionPanel.add(Box.createVerticalGlue());
        dataCollectionPanel.add(promptTeacherName, LEFT_ALIGNMENT);
        dataCollectionPanel.add(Box.createRigidArea(new Dimension(0,5)));
        dataCollectionPanel.add(enterTeacherName);
        dataCollectionPanel.add(Box.createRigidArea(new Dimension(0,20)));
        dataCollectionPanel.add(Box.createVerticalGlue());
        dataCollectionPanel.add(promptNumOfSessions, LEFT_ALIGNMENT);
        dataCollectionPanel.add(Box.createRigidArea(new Dimension(0,5)));
        dataCollectionPanel.add(enterNumOfSessions);
        dataCollectionPanel.add(Box.createRigidArea(new Dimension(0,20)));
        dataCollectionPanel.add(Box.createVerticalGlue());
        dataCollectionPanel.setVisible(true);
    }

}
