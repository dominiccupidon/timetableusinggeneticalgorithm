package ui.screens;

import model.CourseDatabase;
import model.TimeTableModel;
import model.scheduler.GeneticAlgorithm;
import persistence.JsonWriter;
import validation.DuplicateException;
import validation.OutOfRangeException;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.swing.*;
import javax.swing.table.TableCellRenderer;
import java.awt.*;
import java.io.File;
import java.io.IOException;

import static javax.swing.JOptionPane.showMessageDialog;

//Responsible for graphically handling the time table
public class TimeTableTab extends JPanel {
    private static final String FILENAME = "TimeTable.json";
    private static final String ERROR_SOUND = "./data/ErrorSound.wav";

    private JTable timeTable;

    private JScrollPane tablePanel;
    private JPanel buttonPanel;
    private JLabel header;

    private JButton createTimeTable;
    private JButton saveTimeTable;

    private TimeTableModel tableModel;

    private Clip clip;
    private AudioInputStream audioInputStream;

    public TimeTableTab() {
        super();
        initGraphics();
        super.setVisible(true);
    }

    private void initGraphics() {
        initSound();
        initTable();
        initTablePanel();
        initButtonPanel();
        initHeader();
        initMainPanel();
    }

    private void initHeader() {
        header = new JLabel("Time Table");
        header.setFont(header.getFont().deriveFont(24f));
    }

    private void initTable() {
        CustomCellRender cellRender = new CustomCellRender();
        tableModel = new TimeTableModel();
        timeTable = new JTable(tableModel);
        timeTable.setSelectionMode(ListSelectionModel.SINGLE_INTERVAL_SELECTION);
        timeTable.setDefaultRenderer(Object.class, cellRender);
        timeTable.setRowHeight(60);
        timeTable.setFillsViewportHeight(true);
        timeTable.setVisible(true);
    }

    private void initTablePanel() {
        tablePanel = new JScrollPane(timeTable,
                JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
                JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
        tablePanel.setVisible(true);
    }

    private void initButtonPanel() {
        buttonPanel = new JPanel();
        buttonPanel.setLayout(new BoxLayout(buttonPanel, BoxLayout.LINE_AXIS));
        initButtons();
        buttonPanel.add(Box.createHorizontalGlue());
        buttonPanel.add(createTimeTable);
        buttonPanel.add(Box.createRigidArea(new Dimension(50, 0)));
        buttonPanel.add(saveTimeTable);
        buttonPanel.setVisible(true);
    }

    private void initButtons() {
        initCreateTimeTableButton();
        initSaveTimeTableButton();
    }

    private void initCreateTimeTableButton() {
        createTimeTable = new JButton("Create Time Table");
        createTimeTable.addActionListener(e -> {
            runGeneticAlgorithm();
            timeTable.setModel(tableModel);
        });
    }

    private void initSaveTimeTableButton() {
        saveTimeTable = new JButton("Save Time Table");
        saveTimeTable.addActionListener(e -> {
            try {
                JsonWriter writer = new JsonWriter(tableModel, FILENAME);
                writer.writeToJsonFile();
            } catch (IOException io) {
                playErrorSound();
                showMessageDialog(this, io.getMessage(),
                        "File Error", JOptionPane.ERROR_MESSAGE);
            }
        });
    }

    private void initMainPanel() {
        super.setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));
        super.setBorder(BorderFactory.createEmptyBorder(30, 50, 50, 50));
        super.add(header);
        super.add(Box.createRigidArea(new Dimension(0, 10)));
        super.add(tablePanel);
        super.add(Box.createRigidArea(new Dimension(0, 25)));
        super.add(buttonPanel);
        super.setVisible(true);
    }

    private void runGeneticAlgorithm() {
        GeneticAlgorithm algorithm;
        MainScreen ui = (MainScreen) SwingUtilities.getAncestorOfClass(MainScreen.class, this);
        CourseDatabase database = new CourseDatabase();
        try {
            database.updateDatabaseWithModel(ui.getTableModel());
        } catch (OutOfRangeException | DuplicateException e) {
            playErrorSound();
            showMessageDialog(this,
                    "Error in creating the timetable please check Course Data",
                    "Error", JOptionPane.ERROR_MESSAGE);
        }
        algorithm = new GeneticAlgorithm(database);
        tableModel = new TimeTableModel(algorithm.getSolution());
    }

    private void initSound() {
        try {
            audioInputStream = AudioSystem.getAudioInputStream(new File(ERROR_SOUND).getAbsoluteFile());
            clip = AudioSystem.getClip();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void playErrorSound() {
        try {
            clip.open(audioInputStream);
        } catch (Exception e) {
            e.printStackTrace();
        }
        clip.start();
    }

    private static class CustomCellRender extends JList<String> implements TableCellRenderer {

        @Override
        public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected,
                                                       boolean hasFocus, int row, int column) {
            if (value instanceof String) {
                String [] cell = ((String) value).split("\n");
                setListData(cell);
            }
            if (isSelected) {
                setBackground(UIManager.getColor("Table.selectionBackground"));
            } else {
                setBackground(UIManager.getColor("Table.background"));
            }

            return this;
        }
    }
}
