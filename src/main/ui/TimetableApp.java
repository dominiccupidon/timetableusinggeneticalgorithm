package ui;

import model.CourseDatabase;
import model.CourseTableModel;
import persistence.JsonReader;
import persistence.JsonWriter;
import ui.screens.DataCollectionScreen;
import ui.screens.MainScreen;
import validation.DuplicateException;
import validation.OutOfRangeException;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.swing.*;
import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.IOException;

import static java.lang.System.exit;
import static javax.swing.JOptionPane.showMessageDialog;
import static javax.swing.JOptionPane.showOptionDialog;

//Main User Interface for the application
//TODO: Refine interface and audiovisuals
public class TimetableApp {
    private static final String FILENAME = "Database.json";
    private static final String ERROR_SOUND = "./data/ErrorSound.wav";
    public static final int WIDTH = 800;
    public static final int HEIGHT = 500;
    private JFrame ui;
    private Clip clip;
    private AudioInputStream audioInputStream;
    private CourseDatabase database;
    public static MainScreen mainScreen;

    /*
    EFFECTS: Runs timetable application.
     */
    public TimetableApp() {
        database = new CourseDatabase();
        ui = new JFrame("Timetable");
        initFrame();
        dataCollectionMenu();
    }

    /*
      MODIFIES: this
      EFFECTS:  Initialises the GUI and starts the process of saving the course database to a file.
                Plays error sound if the process of saving the database goes wrong.
     */
    private void initFrame() {
        initSound();
        ui.setLayout(new CardLayout());
        ui.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                if (mainScreen != null) {
                    try {
                        updateDatabase();
                        saveDatabaseToFile();
                        super.windowClosing(e);
                    } catch (OutOfRangeException | DuplicateException ex) {
                        playErrorSound();
                        showMessageDialog(ui,
                                "Invalid ID found in database, this file cannot be used to load save data from",
                                "Invalid ID ", JOptionPane.ERROR_MESSAGE);
                    }
                }
            }
        });
        ui.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        ui.setVisible(false);
    }

    /*
    MODIFIES: this and choice
    EFFECTS:  Records the user's choice and takes that value to determine what
              action the programme should take next.
    */
    private void dataCollectionMenu() {
        final String [] options = {"Enter course data manually", "Read course data from file"};
        int choice;

        choice = showOptionDialog(ui, "How would you like the data to be collected",
                "Data Collection", JOptionPane.YES_NO_CANCEL_OPTION,
                JOptionPane.QUESTION_MESSAGE, null, options, options[0]);
        switch (choice) {
            case JOptionPane.YES_OPTION: ui.setSize(WIDTH, HEIGHT + 250);
                ui.setContentPane(new DataCollectionScreen());
                break;
            case JOptionPane.NO_OPTION: readDatabaseFromFile();
                ui.setSize(WIDTH, HEIGHT);
                mainScreen = new MainScreen(database);
                ui.setContentPane(mainScreen);
                break;
            case JOptionPane.CLOSED_OPTION: exit(0);
                break;
        }
        ui.setVisible(true);
    }

    /*
      MODIFIES: this, model
      EFFECTS:  Creates and then initialises model, clears database and then updates it with model's data.
                Throws OutOfRangeException or DuplicateException if data from model is of an incorrect format.
     */
    private void updateDatabase() throws OutOfRangeException, DuplicateException {
        CourseTableModel model = mainScreen.getTableModel();
        database.removeAll();
        database.updateDatabaseWithModel(model);
    }

    /*
    MODIFIES: Sets the name of the file to the variable filename and uses writer
              to write the information to the file.
    EFFECTS:  Creates file using information in database. Plays an error sound if the process goes wrong.
     */
    private void saveDatabaseToFile() {
        JsonWriter writer = new JsonWriter(database, FILENAME);
        try {
            writer.writeToJsonFile();
        } catch (IOException io) {
            playErrorSound();
            showMessageDialog(ui, io.getMessage(),
                    "File Error", JOptionPane.ERROR_MESSAGE);
        }
    }

    /*
    REQUIRES: Database.json to exist and have accurate information in it.
    MODIFIES: this
    EFFECTS:  Transfers data from Database.json to database. Plays an error sound if process goes wrong.
     */
    private void readDatabaseFromFile() {
        JsonReader reader = new JsonReader();
        try {
            database = reader.readFromFile(FILENAME);
        } catch (IOException io) {
            playErrorSound();
            showMessageDialog(ui, io.getMessage(),
                    "File Error", JOptionPane.ERROR_MESSAGE);
            ui.setContentPane(new DataCollectionScreen());
        } catch (DuplicateException i) {
            playErrorSound();
            showMessageDialog(ui, "File cannot be used\nPlease enter the data manually",
                    "File Error", JOptionPane.ERROR_MESSAGE);
            ui.setContentPane(new DataCollectionScreen());
        }
    }

    /*
      MODIFIES: this
      EFFECTS:  Initialises the sound.
     */
    private void initSound() {
        try {
            audioInputStream = AudioSystem.getAudioInputStream(new File(ERROR_SOUND).getAbsoluteFile());
            clip = AudioSystem.getClip();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /*
      MODIFIES: this
      EFFECTS:  Plays an error sound.
     */
    private void playErrorSound() {
        try {
            clip.open(audioInputStream);
        } catch (Exception e) {
            e.printStackTrace();
        }
        clip.start();
    }
}
