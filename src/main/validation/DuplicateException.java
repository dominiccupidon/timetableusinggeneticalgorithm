package validation;

/*
    TODO
        Override the getMessage() to print custom exception messages
        for the following situations:
            Duplicate Course ID
            Duplicate Course Name
 */
public class DuplicateException extends Exception {

    public DuplicateException(String message) {
        super(message);
    }
}
