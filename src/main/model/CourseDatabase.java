package model;

import validation.DuplicateException;
import validation.OutOfRangeException;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

//Represents the database used to store course information
public class CourseDatabase implements Iterable<Course> {
    private List<Course> database;

    public CourseDatabase() {
        database = new ArrayList<>();
    }

    /*
      REQUIRES: The course code to be unique to that specific course.
      MODIFIES: this
      EFFECTS:  Adds the course to the database.
     */
    public void addCourse(Course course) throws DuplicateException {
        for (Course course1: database) {
            if (course.getCourseCode() == course1.getCourseCode()) {
                throw new DuplicateException("Course IDs must be unique to the course");
            }
        }
        database.add(course);
    }

    /*
      REQUIRES: To not be an empty list
      MODIFIES: this
      EFFECTS: Removes the course that corresponds to the course code entered.
     */
    public void removeCourse(int code) {
        database.removeIf(course -> course.getCourseCode() == code);
    }

    /*
      MODIFIES: this
      EFFECTS:  Removes all courses from database.
     */
    public void removeAll() {
        database.clear();
    }

    /*
      REQUIRES: model to not be empty.
      MODIFIES: this
      EFFECTS:  Transfers the data from model to database.
                Throws OutOfRangeException or DuplicateException if data from model is of an incorrect format.
     */
    public void updateDatabaseWithModel(CourseTableModel model) throws OutOfRangeException, DuplicateException {
        for (int i = 0; i < model.getRowCount(); i++) {
            addCourse(new Course((Integer) model.getValueAt(i, 0),
                    (String) model.getValueAt(i, 1),
                    (Integer) model.getValueAt(i, 2),
                    (String) model.getValueAt(i, 3),
                    (Integer) model.getValueAt(i, 4),
                    (String) model.getValueAt(i, 5),
                    (Integer) model.getValueAt(i, 6)));
        }
    }

    /*
      REQUIRES: database to not be an empty list and the value of code to refer to an actual course in the database.
      MODIFIES: module
      EFFECTS: Returns the course that corresponds to the course code entered.
     */
    public Course searchByCourseCode(int code) {
        Course module = null;
        for (Course course: database) {
            if (course.getCourseCode() == code) {
                module = course;
            }
        }
        return module;
    }

    /*
      REQUIRES: database to not be an empty list and the value of code to refer to an actual course in the database.
      MODIFIES: list
      EFFECTS: Returns a list of courses that corresponds to the room code entered.
     */
    public ArrayList<Course> searchByRoomCode(int code) {
        ArrayList<Course> list = new ArrayList<>();
        for (Course course: database) {
            if (course.getRoomCode() == code) {
                list.add(course);
            }
        }
        return list;
    }

    public int getLength() {
        return database.size();
    }

    /*
      REQUIRES: Index to be greater than or equal to zero.
      EFFECTS: Returns the course at the position index.
     */
    public Course getCourseUsingPosition(int index) {
        return database.get(index);
    }

    @Override
    public Iterator<Course> iterator() {
        return database.listIterator();
    }
}
