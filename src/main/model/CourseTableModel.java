package model;

import javax.swing.table.DefaultTableModel;

//Holds the information to be displayed in the Course Database Tab
public class CourseTableModel extends DefaultTableModel {
    private boolean editableCell;
    private CourseDatabase database;
    private Object[][] rowData;

     /*
       REQUIRES: database to be a non-empty list
       EFFECTS:  Creates and initialises the data vector and then sets the cells
                 as not being editable
     */
    public CourseTableModel(CourseDatabase database) {
        super();
        String [] columnHeaders = { "Course ID", "Course Name", "Room ID", "Room Name",
                "Teacher ID", "Teacher Name", "Number of Sessions"};
        this.database = database;
        rowData = new Object[database.getLength()][7];
        setDataModel();
        super.setDataVector(rowData, columnHeaders);
        setCellEditable(false);
    }

    /*
      REQUIRES: rowIndex and columnIndex to be non-negative
      EFFECTS:  returns whether or not the cell is editable based on its rowIndex
     */
    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        if (rowIndex != 0) {
            return editableCell;
        } else {
            return false;
        }
    }

    /*
      MODIFIES: this
      EFFECTS:  sets the value of editable
     */
    public void setCellEditable(boolean val) {
        editableCell = val;
    }

    /*
      MODIFIES: this, i and j
      EFFECTS:  transfers the data from database to rowData in a specific order
     */
    private void setDataModel() {
        int i = 0;
        for (Course course : database) {
            for (int j = 0; j < 7; j++) {
                switch (j) {
                    case 0: rowData[i][j] = course.getCourseCode();
                        break;
                    case 1: rowData[i][j] = course.getCourseName();
                        break;
                    case 2: rowData[i][j] = course.getRoomCode();
                        break;
                    case 3: rowData[i][j] = course.getRoomName();
                        break;
                    case 4: rowData[i][j] = course.getTeacherCode();
                        break;
                    case 5: rowData[i][j] = course.getTeacherName();
                        break;
                    case 6: rowData[i][j] = course.getSessions();
                        break;
                }
            }
            ++i;
        }
    }

}
