package model.scheduler;

import model.Course;
import model.CourseDatabase;

import java.util.BitSet;
import java.util.Random;

//Represents a possible solution for the Genetic Algorithm
public class Chromosome implements Comparable<Chromosome> {
    public static final int SLOTS = 55;
    public static final int DAYS = 11;
    private final BitSet chromosome;

    private CourseDatabase database;
    private int fitness;

    /*
     REQUIRES: database to not be an empty list.
     EFFECTS:  Creates a chromosome using a Course Database.
     */
    public Chromosome(CourseDatabase database) {
        this.database = database;
        chromosome = new BitSet(SLOTS * database.getLength());
        fitness = 0;
        encodeCourseDatabase();
    }

    /*
     EFFECTS: Creates a chromosome using a BitSet.
     */
    public Chromosome(BitSet chromosome, CourseDatabase database) {
        this.chromosome = chromosome;
        this.database = database;
        fitness = 0;
    }

    /*
      REQUIRES: chromosome != null
      MODIFIES: this
      EFFECTS:  Evaluates the overall fitness of the chromosome
     */
    public void evalFitness() {
        fitness = 0;
        calculateNumberOfConflicts();
        calculateNumberOfCoursesFoundTwiceOrMoreInADay();
    }

    /*
     REQUIRES: chromosome != null
     MODIFIES: this
     EFFECTS:  Evaluates the fitness of the chromosome based on the number of scheduling
               conflicts
     */
    private void calculateNumberOfConflicts() {
        int size = chromosome.length() / SLOTS;
        BitSet[] subChromosomes = new BitSet[size];
        for (int i = 0; i < size; i++) {
            subChromosomes[i] = chromosome.get(((SLOTS * i)), ((SLOTS - 1) + (SLOTS * i)));
        }
        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                BitSet subChromosome = chromosome.get(((SLOTS * i)), ((SLOTS - 1) + (SLOTS * i)));
                if (i != j) {
                    subChromosome.and(subChromosomes[j]);
                    fitness += subChromosome.cardinality();
                }
            }
        }
    }

    /*
     REQUIRES: chromosome != null
     MODIFIES: this
     EFFECTS:  Evaluates the fitness of the chromosome based on the number of times a course
               is scheduled per day.
     */
    private void calculateNumberOfCoursesFoundTwiceOrMoreInADay() {
        BitSet subChromosome;
        for (int i = 0; i < chromosome.length() / DAYS; i++) {
            subChromosome = chromosome.get(((DAYS * i)), ((DAYS - 1) + (DAYS * i)));
            if (subChromosome.cardinality() > 1) {
                fitness += subChromosome.cardinality();
            }
        }
    }

    /*
     REQUIRES: database to be a non-empty list.
     MODIFIES: this
     EFFECTS:  Encodes the database into a BitSet.
     */
    private void encodeCourseDatabase() {
        int i = 0;
        for (Course course : database) {
            for (int j = 0; j < course.getSessions(); j++) {
                Random random = new Random();
                int slot = random.nextInt(SLOTS);
                while (chromosome.get(slot + (SLOTS * i))) {
                    slot = random.nextInt(SLOTS);
                }
                chromosome.set(slot + (SLOTS * i));
            }
            ++i;
        }
    }

    /*
     REQUIRES: chromosome != null
     MODIFIES: this
     EFFECTS:  Mutates a single point within chromosome, but ensures that the number of
               bits set to true remains constant.
     */
    public void mutate() {
        int numOfSubChromosomes = chromosome.size() / SLOTS;
        int subChromosome = new Random().nextInt(numOfSubChromosomes);
        int slot = new Random().nextInt(SLOTS);
        int flipped;
        if (chromosome.get(slot + (SLOTS * subChromosome))) {
            if (chromosome.nextClearBit(slot + (SLOTS * subChromosome)) > (SLOTS * (subChromosome + 1))) {
                flipped = chromosome.previousClearBit(slot + (SLOTS * subChromosome));
            } else {
                flipped = chromosome.nextClearBit(slot + (SLOTS * subChromosome));
            }
        } else {
            if (chromosome.nextSetBit(slot + (SLOTS * subChromosome)) > (SLOTS * (subChromosome + 1))) {
                flipped = chromosome.previousSetBit(slot + (SLOTS * subChromosome));
            } else {
                flipped = chromosome.nextSetBit(slot + (SLOTS * subChromosome));
            }
        }
        if (flipped > 0) {
            chromosome.flip(flipped);
            chromosome.flip(slot + (SLOTS * subChromosome));
        }
    }

    /*
     MODIFIES: courses, i and pos
     EFFECTS:  Converts the information in chromosome into a form useful to the
               TimeTableModel class.
     */
    public String[] decodeChromosome() {
        String [] courses = new String[SLOTS];
        for (int i = 0; i < SLOTS; i++) {
            courses [i] = " ";
        }
        for (int i = 0; i < chromosome.length(); i++) {
            if (chromosome.get(i)) {
                int pos = i % SLOTS;
                courses [pos] = database.getCourseUsingPosition(i / SLOTS).toString();
            }
        }
        return courses;
    }

    /*
     EFFECTS: Returns the fitness of the chromosome.
     */
    public int getFitness() {
        return fitness;
    }

    /*
    EFFECTS: Returns a section of chromosome as specified.
     */
    public BitSet getChromosome(int start, int end) {
        return chromosome.get(start, end);
    }

    /*
     EFFECTS: Returns the course database.
     */
    public CourseDatabase getDatabase() {
        return database;
    }

    /*
     EFFECTS: Returns the size of the chromosome
     */
    public int getSize() {
        return chromosome.size();
    }

    /*
     EFFECTS: Returns how many slots are filled in
     */
    public int getCardinality() {
        return chromosome.cardinality();
    }

    @Override
    public int compareTo(Chromosome c) {
        return Integer.compare(this.fitness, c.fitness);
    }
}
