package model.scheduler;

import model.Course;
import model.CourseDatabase;

import java.util.ArrayList;
import java.util.BitSet;
import java.util.Collections;
import java.util.Random;

//TODO Test the class
public class Population {
    private static final int INITIAL_SIZE = 55;
    private ArrayList<Chromosome> population;
    private CourseDatabase database;

    /*
     REQUIRES: database to be a non-empty list.
     EFFECTS:  Uses the data in database to initialise population.
     */
    public Population(CourseDatabase database) {
        population = new ArrayList<>(INITIAL_SIZE);
        this.database = database;
        createPopulation(database);
    }

    /*
     REQUIRES: population to be a non-empty list.
     EFFECTS:  Uses the data in population to initialise this.population.
     */
    public Population(ArrayList<Chromosome> population) {
        this.population = population;
    }

    /*
     MODIFIES: this
     EFFECTS:  Creates an initial population of size INITIAL_SIZE and stores them in chromosomes.
     */
    private void createPopulation(CourseDatabase database) {
        for (int i = 0; i < INITIAL_SIZE; i++) {
            Chromosome chromosome = new Chromosome(database);
            population.add(chromosome);
        }
    }

    /*
     MODIFIES: this
     EFFECTS:  Mutates the Chromosomes.
     */
    public void mutateChromosomes() {
        Random random = new Random();
        for (int i = 0; i < 5; i++) {
            population.get(random.nextInt(population.size())).mutate();
        }
    }

    /*
     MODIFIES: this
     EFFECTS:  Creates new Chromosomes by combining different of sections of two Chromosomes.
     */
    public void crossoverChromosomes() {
        Random selectChromosome = new Random();
        Random selectPosition = new Random();
        int size = population.get(0).getSize();
        for (int i = 0; i < 30; i++) {
            int pos = selectPosition.nextInt(size / 2);
            BitSet mother = population.get(selectChromosome.nextInt(population.size())).getChromosome(0, pos);
            BitSet father = population.get(selectChromosome.nextInt(population.size())).getChromosome(pos + 1, size);
            BitSet child = new BitSet(size);
            child.or(mother);
            ++pos;
            while (pos < size) {
                int j = 0;
                child.set(pos, father.get(j));
                ++j;
                ++pos;
            }
            Chromosome chromosome = new Chromosome(child, database);
            population.add(chromosome);
        }
    }

    /*
     MODIFIES: this
     EFFECTS:  Returns a list of the best Chromosomes based on fitness
     */
    public ArrayList<Chromosome> selectChromosomes() {
        sortChromosome();
        return new ArrayList<>(population.subList(0, 25));
    }

    /*
      MODIFIES: this
      EFFECTS:  Returns the best Chromosome based on fitness
     */
    public Chromosome bestChromosome() {
        sortChromosome();
        return population.get(0);
    }

    /*
      MODIFIES: this
      EFFECTS:  Organises the population based on fitness
     */
    private void sortChromosome() {
        int expectedCardinality = 0;
        CourseDatabase database = population.get(0).getDatabase();
        for (Course course : database) {
            expectedCardinality += course.getSessions();
        }
        for (Chromosome chromosome : population) {
            chromosome.evalFitness();
        }
        int finalExpectedCardinality = expectedCardinality;
        population.removeIf(chromosome -> chromosome.getCardinality() != finalExpectedCardinality);
        Collections.sort(population);
    }
}
