package model.scheduler;

import model.CourseDatabase;

import java.util.ArrayList;

public class GeneticAlgorithm {
    public static final int GENERATIONS = 10;
    private Chromosome bestChromosome;

    /*
     REQUIRES: database to be a non-empty list.
     EFFECTS:  Uses the data in database to start the algorithm.
     */
    public GeneticAlgorithm(CourseDatabase database) {
        runGenerations(database);
    }

    /*
    MODIFIES: this
    EFFECTS:  Performs the genetic algorithm.
     */
    private void runGenerations(CourseDatabase database) {
        Population population = new Population(database);
        for (int i = 0; i < GENERATIONS; i++) {
            population.mutateChromosomes();
            population.crossoverChromosomes();
            ArrayList<Chromosome> chromosomes = population.selectChromosomes();
            population = new Population(chromosomes);
        }
        bestChromosome = population.bestChromosome();
    }

    /*
     EFFECTS: Gets the optimal solution produced by the algorithm.
     */
    public Chromosome getSolution() {
        return bestChromosome;
    }
}
