package model;

import model.scheduler.Chromosome;

import javax.swing.table.DefaultTableModel;

//Holds the information that is displayed in the Time Table Tab
public class TimeTableModel extends DefaultTableModel {
    private Object[][] rowData;
    private Chromosome chromosome;
    private boolean editableCell;
    private  String [] columnHeaders = { "Time", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday"};
    private  String [] rowHeaders = {"8:00", "9:00", "10:00", "11:00", "12:00",
            "1:00", "2:00", "3:00", "4:00", "5:00", "6:00"};

    /*
      REQUIRES: chromosome to be a non-null variable
      EFFECTS:  Creates and initialises the data vector and then sets the
                cells as not being editable
    */
    public TimeTableModel(Chromosome chromosome) {
        super();

        this.chromosome = chromosome;
        rowData = new Object[11][6];
        setCellEditable(false);
        setDataModelWithData();
        super.setDataVector(rowData, columnHeaders);
    }

    /*
      EFFECTS:  Creates an empty data vector and sets the cells as not being editable
    */
    public TimeTableModel() {
        super();
        rowData = new Object[11][6];
        setCellEditable(false);
        setDataModelWithoutData();
        super.setDataVector(rowData, columnHeaders);
    }

    /*
      REQUIRES: rowIndex and columnIndex to be non-negative
      EFFECTS:  returns whether or not the cell is editable based on its rowIndex and columnIndex
     */
    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        if ((rowIndex != 0) && (columnIndex != 0)) {
            return editableCell;
        } else {
            return false;
        }
    }

    /*
      MODIFIES: this
      EFFECTS:  sets the value of editable
     */
    public void setCellEditable(boolean val) {
        editableCell = val;
    }

    /*
      MODIFIES: this, k and j
      EFFECTS:  transfers the data from chromosome to rowData in a specific order
     */
    private void setDataModelWithData() {
        String [] courses =  chromosome.decodeChromosome();
        for (int k = 0; k < 6; k++) {
            for (int j = 0; j < 11; j++) {
                if (k == 0) {
                    rowData [j][k] = rowHeaders[j];
                } else {
                    rowData [j][k] = courses [((k - 1) * Chromosome.DAYS) + j];
                }
            }
        }
    }

    /*
      MODIFIES: this, k and j
      EFFECTS:  initialises the elements to rowData
     */
    private void setDataModelWithoutData() {
        for (int j = 0; j < 11; j++) {
            for (int k = 0; k < 6; k++) {
                if (k == 0) {
                    rowData [j][k] = rowHeaders[j];
                } else {
                    rowData [j][k] = " ";
                }
            }
        }
    }
}
