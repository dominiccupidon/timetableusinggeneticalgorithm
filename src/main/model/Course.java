package model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import validation.OutOfRangeException;

//Represents one course that is stored within the database
public class Course {
    static final int MIN_SESSIONS_PER_WEEK = 1;
    static final int MAX_SESSIONS_PER_WEEK = 4;
    static final int FIRST_CODE = 1000;
    static final int LAST_CODE = 9999;

    private int courseCode;
    private String courseName;
    private int roomCode;
    private String roomName;
    private int teacherCode;
    private String teacherName;
    private int sessions;

    /*
      EFFECTS:  Sets the courseCode, courseName, roomCode, roomName, teacherCode, teacherName, and sessions
    */
    @JsonCreator
    public Course(@JsonProperty("courseCode") int courseCode,
                  @JsonProperty("courseName") String courseName,
                  @JsonProperty("roomCode") int roomCode,
                  @JsonProperty("roomName") String roomName,
                  @JsonProperty("teacherCode") int teacherCode,
                  @JsonProperty("teacherName") String teacherName,
                  @JsonProperty("sessions") int sessions) throws OutOfRangeException  {
        setCourseName(courseName);
        setCourseCode(courseCode);
        setRoomName(roomName);
        setRoomCode(roomCode);
        setTeacherCode(teacherCode);
        setTeacherName(teacherName);
        setSessions(sessions);
    }

    /*
      MODIFIES: this
      EFFECTS:  The course code is set to code. Throws OutOfRangeException if code is out of range.
     */
    public void setCourseCode(int code) throws OutOfRangeException {
        if ((FIRST_CODE <= code) && (code <= LAST_CODE)) {
            this.courseCode = code;
        } else {
            throw new OutOfRangeException("IDs must be between 1000 and 9999 inclusive");
        }
    }

    /*
      MODIFIES: this
      EFFECTS:  The course name is set to name.
    */
    public void setCourseName(String name) {
        this.courseName = name;
    }

    /*
      EFFECTS: Returns the value of the course code.
     */
    public int getCourseCode() {
        return courseCode;
    }

    /*
      EFFECTS: Returns the value of the course name.
     */
    public String getCourseName() {
        return courseName;
    }

    /*
      MODIFIES: this
      EFFECTS:  The room code is set to code. Throws OutOfRangeException if code is out of range.
    */
    public void setRoomCode(int code) throws OutOfRangeException {
        if ((FIRST_CODE <= code) && (code <= LAST_CODE)) {
            this.roomCode = code;
        } else {
            throw new OutOfRangeException("IDs must be between 1000 and 9999 inclusive");
        }
    }

    /*
      MODIFIES: this
      EFFECTS:  The room name is set to name.
    */
    public void setRoomName(String name) {
        this.roomName = name;
    }

    /*
      EFFECTS: Returns the value of the room code.
     */
    public int getRoomCode() {
        return roomCode;
    }

    /*
      EFFECTS: Returns the room name.
     */
    public String getRoomName() {
        return roomName;
    }

    /*
      MODIFIES: this
      EFFECTS:  The teacher code is set to code. Throws OutOfRangeException if code is out of range.
    */
    public void setTeacherCode(int code) throws OutOfRangeException {
        if ((FIRST_CODE <=  code) && (code <= LAST_CODE)) {
            this.teacherCode = code;
        } else {
            throw new OutOfRangeException("IDs must be between 1000 and 9999 inclusive");
        }
    }

    /*
      MODIFIES: this
      EFFECTS:  The teacher name is set to name.
    */
    public void setTeacherName(String name) {
        this.teacherName =  name;
    }

    /*
      EFFECTS: Returns the value of the teacher code.
     */
    public int getTeacherCode() {
        return teacherCode;
    }

    /*
      EFFECTS: Returns the teacher name.
     */
    public String getTeacherName() {
        return teacherName;
    }

    /*
      MODIFIES: this
      EFFECTS:  The number of sessions per week is set. Throws OutOfRangeException if sessions is out of range.
    */
    public void setSessions(int sessions) throws OutOfRangeException {
        if ((MIN_SESSIONS_PER_WEEK <= sessions) && (sessions <= MAX_SESSIONS_PER_WEEK)) {
            this.sessions = sessions;
        } else {
            throw new OutOfRangeException("The number of sessions must be between 1 and 4, inclusive");
        }
    }

    /*
      EFFECTS: Returns the number of sessions per week
     */
    public int getSessions() {
        return sessions;
    }

    @Override
    public String toString() {
        return courseName + "\n" + roomName + "\n" + teacherName;
    }
}
