package persistence;

import com.fasterxml.jackson.databind.ObjectMapper;
import model.Course;
import model.CourseDatabase;
import validation.DuplicateException;

import java.io.IOException;
import java.nio.file.Paths;

public class JsonReader {
    private static final String FILE_PATH = "./data/";

    public JsonReader() {}

    /*
    REQUIRES: The file to be non-empty and exist
    EFFECTS: Outputs a database of courses from json file
     */
    public CourseDatabase readFromFile(String filename) throws IOException, DuplicateException {
        ObjectMapper mapper = new ObjectMapper();
        CourseDatabase database = new CourseDatabase();
        Course[] courseList = mapper.readValue(Paths.get(FILE_PATH + filename).toFile(), Course[].class);
        for (Course course : courseList) {
            database.addCourse(course);
        }
        return database;
    }

}
