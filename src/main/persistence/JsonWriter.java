package persistence;


import com.fasterxml.jackson.core.util.DefaultPrettyPrinter;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import model.Course;
import model.CourseDatabase;

import javax.swing.table.DefaultTableModel;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

public class JsonWriter {
    private static final String FILE_PATH = "./data/";
    private CourseDatabase database;
    private DefaultTableModel model;
    private String filename;


    public JsonWriter(CourseDatabase database, String filename) {
        this.database = database;
        this.filename = filename;
    }

    public JsonWriter(DefaultTableModel model, String filename) {
        this.model = model;
        this.filename = filename;
    }

    /*
    REQUIRES: Either database or model to be non-empty and filename to be properly initialised.
    MODIFIES: mapper
    EFFECTS:  Uses the information in either database or model to create a file.
     */
    public void writeToJsonFile() throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        ObjectWriter writer = mapper.writer(new DefaultPrettyPrinter());
        List<Course> courseList = new ArrayList<>();
        if (database != null) {
            for (Course course : database) {
                courseList.add(course);
            }
            writer.writeValue(Paths.get(FILE_PATH + filename).toFile(), courseList);
        }
        if (model != null) {
            Vector vector = model.getDataVector();
            Object [] strings = vector.toArray();
            writer.writeValue(Paths.get(FILE_PATH + filename).toFile(), strings);
        }
    }

    public String getFileName() {
        return filename;
    }
}
