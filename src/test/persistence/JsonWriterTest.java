package persistence;

import model.Course;
import model.CourseDatabase;
import model.TimeTableModel;
import model.scheduler.GeneticAlgorithm;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import validation.DuplicateException;
import validation.OutOfRangeException;

import javax.swing.table.DefaultTableModel;
import java.io.IOException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

public class JsonWriterTest {
    private static final String TEST_WRITE_DATABASE_JSON = "testWriteDatabase.json";
    private static final String TEST_WRITE_TIME_TABLE_JSON = "testWriteTimeTable.json";
    private JsonWriter writer;

    @BeforeEach
    public void runBefore() {
        CourseDatabase database = new CourseDatabase();
        for (int i = 0; i < 5; i++) {
            try {
                database.addCourse(new Course(1000 + i, "MATH", 7516 + i, "Room " + i, 8994, "Mr. Brown", 3));
            } catch (OutOfRangeException | DuplicateException o) {
                fail("No exceptions should have been thrown");
            }
        }
       writer = new JsonWriter(database, TEST_WRITE_DATABASE_JSON);
    }

    @Test
    public void testConstructor() {
        assertEquals(TEST_WRITE_DATABASE_JSON, writer.getFileName());
    }

    @Test
    public void testWriteDatabaseToJsonFile() {
        JsonReader reader = new JsonReader();
        CourseDatabase database;

        try {
            writer.writeToJsonFile();
            database = reader.readFromFile(TEST_WRITE_DATABASE_JSON);
            for (int i = 0; i < database.getLength(); i++) {
                Course course = new Course(1000 + i, "MATH", 7516 + i, "Room " + i, 8994, "Mr. Brown", 3);
                assertEquals(course.getCourseCode(), database.searchByCourseCode(1000 + i).getCourseCode());
                assertEquals(course.getCourseName(), database.searchByCourseCode(1000 + i).getCourseName());
                assertEquals(course.getRoomCode(), database.searchByCourseCode(1000 + i).getRoomCode());
                assertEquals(course.getRoomName(), database.searchByCourseCode(1000 + i).getRoomName());
                assertEquals(course.getTeacherCode(), database.searchByCourseCode(1000 + i).getTeacherCode());
                assertEquals(course.getTeacherName(), database.searchByCourseCode(1000 + i).getTeacherName());
                assertEquals(course.getSessions(), database.searchByCourseCode(1000 + i).getSessions());
            }
        } catch (IOException | OutOfRangeException | DuplicateException o) {
            fail("No exceptions should have been thrown");
        }
    }

    @Test
    public void testWriteTimeTableToJsonFile() {
        JsonWriter otherWriter = new JsonWriter(createTestData(), TEST_WRITE_TIME_TABLE_JSON);
        try {
            otherWriter.writeToJsonFile();
            assertEquals(TEST_WRITE_TIME_TABLE_JSON, otherWriter.getFileName());
        } catch (IOException io) {
            fail("No exceptions should have been thrown");
        }
    }

    private DefaultTableModel createTestData() {
        CourseDatabase courseDatabase = new CourseDatabase();
        for (int i = 0; i < 5; i++) {
            try {
                courseDatabase.addCourse(new Course(1000 + i, "MATH", 7516 + i, "Room " + i, 8994, "Mr. Brown", 3));
            } catch (OutOfRangeException | DuplicateException o) {
                fail("No exceptions should have been thrown");
            }
        }
        GeneticAlgorithm algorithm = new GeneticAlgorithm(courseDatabase);
        return new TimeTableModel(algorithm.getSolution());
    }
}
