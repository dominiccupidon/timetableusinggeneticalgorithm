package persistence;

import model.Course;
import model.CourseDatabase;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import validation.DuplicateException;
import validation.OutOfRangeException;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

public class JsonReaderTest {
    private static final String TEST_FILE_PASS = "testReadDatabase.json";
    private static final String TEST_FILE_FAIL = "test.json";
    private JsonReader reader;

    @BeforeEach
    public void runBefore() {
        reader = new JsonReader();
    }

    @Test
    public void testReadFromJsonFileThatExists() {
        CourseDatabase database;

        try {
            database = reader.readFromFile(TEST_FILE_PASS);
            for (int i = 0; i < database.getLength(); i++) {
                Course course = new Course(2000 + i, "ENGL", 8917 + i, "Room " + (10 + i), 1983, "Mr. Richards", 3);
                assertEquals(course.getCourseCode(), database.searchByCourseCode(2000 + i).getCourseCode());
                assertEquals(course.getCourseName(), database.searchByCourseCode(2000 + i).getCourseName());
                assertEquals(course.getRoomCode(), database.searchByCourseCode(2000 + i).getRoomCode());
                assertEquals(course.getRoomName(), database.searchByCourseCode(2000 + i).getRoomName());
                assertEquals(course.getTeacherCode(), database.searchByCourseCode(2000 + i).getTeacherCode());
                assertEquals(course.getTeacherName(), database.searchByCourseCode(2000 + i).getTeacherName());
                assertEquals(course.getSessions(), database.searchByCourseCode(2000 + i).getSessions());
            }
        } catch (IOException | OutOfRangeException | DuplicateException o) {
            fail("No exceptions should have been thrown");
        }
    }

    @Test
    public void testReadFromJsonFileThatDoesNotExist() {
        try {
            CourseDatabase database = reader.readFromFile(TEST_FILE_FAIL);
            System.out.println(database.getLength());
            fail("IOException should have been caught");
        } catch (IOException io) {
            io.printStackTrace();
        } catch (DuplicateException i) {
            fail("DuplicateIDException should not have been thrown");
        }
    }
}
