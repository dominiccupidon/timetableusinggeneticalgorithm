package model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import validation.DuplicateException;
import validation.OutOfRangeException;

import static org.junit.jupiter.api.Assertions.*;

public class CourseTableModelTest {
    private CourseTableModel tableModel;
    private CourseDatabase courseDatabase;
    private static final String [] columnHeaders = {
            "Course ID",
            "Course Name",
            "Room ID",
            "Room Name"
    };

    @BeforeEach
    public void runBefore() {
        courseDatabase = new CourseDatabase();
        createTestDatabase();
        tableModel = new CourseTableModel(courseDatabase);
    }

    @Test
    public void testConstructor() {
        assertEquals(columnHeaders[0], tableModel.getColumnName(0));
        assertEquals(columnHeaders[1], tableModel.getColumnName(1));
        assertEquals(columnHeaders[2], tableModel.getColumnName(2));
        assertEquals(columnHeaders[3], tableModel.getColumnName(3));
    }

    @Test
    public void testSetCellEditable() {
        assertFalse(tableModel.isCellEditable(0, 3));
        tableModel.setCellEditable(true);
        assertFalse(tableModel.isCellEditable(0, 1));
        assertTrue(tableModel.isCellEditable(3, 3));
        tableModel.setCellEditable(false);
        assertFalse(tableModel.isCellEditable(1, 3));
    }

    private void createTestDatabase() {
        for (int i = 0; i < 5; i++) {
            try {
                Course course = new Course(1000 + i, "MATH " + i, 4657 + i, "Math Building", 1983, "Mr. Richards", 3);
                courseDatabase.addCourse(course);
            } catch (OutOfRangeException | DuplicateException e) {
                fail("No exception was to be thrown");
            }
        }
    }
}
