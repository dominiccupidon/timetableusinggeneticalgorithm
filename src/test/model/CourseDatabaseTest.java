package model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import validation.DuplicateException;
import validation.OutOfRangeException;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

public class CourseDatabaseTest {
    private  CourseDatabase database;

    @BeforeEach
    public void runBefore() {
        database = new CourseDatabase();
    }

    @Test
    public void testAddSingleCourse() {
        try {
            Course course = new Course(6713, "PHYS 192", 7829, "Hebb Building", 9422, "Mr. Abraham", 2);
            database.addCourse(course);
            assertEquals(1, database.getLength());
            assertEquals(course, database.searchByCourseCode(6713));
        } catch (OutOfRangeException | DuplicateException o) {
            fail("No exception was to be thrown");
        }
    }

    @Test
    public void testAddMultipleCoursesUniqueIDs() {
        try {
            Course course1 = new Course(1933, "CHEM 178", 5029, "Chemistry Building", 3312, "Ms. Thomas", 1);
            database.addCourse(course1);
            assertEquals(1, database.getLength());
            Course course2  = new Course(9881, "CPSC 201", 7112, "Computer Science Lab", 5690, "Ms. Richards", 4);
            database.addCourse(course2);
            assertEquals(course1, database.searchByCourseCode(1933));
            assertEquals(course2, database.searchByCourseCode(9881));
            assertEquals(2, database.getLength());
        } catch (OutOfRangeException | DuplicateException o) {
            fail("No exception was to be thrown");
        }
    }

    @Test
    public void testAddMultipleCoursesDuplicateIDs() {
        try {
            Course course1 = new Course(1933, "CHEM 178", 5029, "Chemistry Building", 3312, "Ms. Thomas", 3);
            try {
                database.addCourse(course1);
            } catch (DuplicateException i) {
                fail("No exception was to be thrown");
            }
            assertEquals(1, database.getLength());
            Course course2  = new Course(1933, "CPSC 201", 7112, "Computer Science Lab", 5690, "Ms. Richards", 2);
            try {
                database.addCourse(course2);
                fail("An exception was to be caught");
            } catch (DuplicateException i) {
                i.printStackTrace();
            }
            finally {
                assertEquals(course1, database.searchByCourseCode(1933));
                assertNotEquals(course2, database.searchByCourseCode(1933));
                assertEquals(1, database.getLength());
            }
        } catch (OutOfRangeException o) {
            fail("No exception was to be thrown");
        }
    }

    @Test
    public void testRemoveSingleCourse() {
        try {
            Course course = new Course(3453, "GEOG 118", 2929, "Geography Building", 7134, "Mr. Smith", 3);
            database.addCourse(course);
            assertEquals(1, database.getLength());
            assertEquals(course, database.searchByCourseCode(3453));
            database.removeCourse(3453);
            assertEquals(0, database.getLength());
        } catch (OutOfRangeException | DuplicateException o) {
            fail("No exception was to be thrown");
        }
    }

    @Test
    public void testRemoveMultipleCourses() {
        try {
            Course course1 = new Course(1023, "BIOL 204", 9321, "Biological Sciences", 7814, "Mrs. Richards", 2);
            database.addCourse(course1);
            Course course2  = new Course(9081, "ART 192", 5719, "Buchanan Building", 8994, "Mr. Brown", 3);
            database.addCourse(course2);
            assertEquals(2, database.getLength());
            assertEquals(course1, database.searchByCourseCode(1023));
            assertEquals(course2, database.searchByCourseCode(9081));
            database.removeCourse(9081);
            assertEquals(1, database.getLength());
            database.removeCourse(1023);
            assertEquals(0, database.getLength());
        } catch (OutOfRangeException | DuplicateException o) {
            fail("No exception was to be thrown");
        }
    }

    @Test
    public void testRemoveAllCoursesAtOnce() {
        try {
            Course course1 = new Course(1993, "BIOL 334", 9321, "Biological Sciences", 7814, "Mrs. Richards", 4);
            database.addCourse(course1);
            Course course2  = new Course(3481, "POLI 200", 5719, "Buchanan Building", 8994, "Mr. Brown", 2);
            database.addCourse(course2);
            assertEquals(2, database.getLength());
            database.removeAll();
            assertEquals(0, database.getLength());
        } catch (OutOfRangeException | DuplicateException o) {
            fail("No exception was to be thrown");
        }
    }

    @Test
    public void testUpdateDatabaseWithModel() {
        try {
            database.updateDatabaseWithModel(createModel());
            assertEquals(3, database.getLength());
        } catch (OutOfRangeException | DuplicateException e) {
            fail("No exception was to be thrown");
        }
    }

    @Test
    public void testSearchByRoomCodeThatExists() {
        ArrayList<Course> list = new ArrayList<>();
        try {
            Course course1 = new Course(1023, "BIOL 204", 9321, "Biological Sciences", 7814, "Mrs. Richards", 1);
            database.addCourse(course1);
            Course course2 = new Course(7092, "BIOL 192", 9321, "Biological Sciences", 7814, "Mrs. Richards", 3);
            database.addCourse(course2);
            list.add(course1);
            list.add(course2);
            assertEquals(list, database.searchByRoomCode(9321));
        } catch (OutOfRangeException | DuplicateException o) {
            fail("No exception was to be thrown");
        }
    }

    @Test
    public void testSearchByRoomCodeThatDoesNotExist() {
        ArrayList<Course> list = new ArrayList<>();
        try {
            Course course1 = new Course(5633, "CHEM 311", 5029, "Chemistry Building", 3312, "Ms. Thomas", 2);
            database.addCourse(course1);
            list.add(course1);
            assertNotEquals(list, database.searchByRoomCode(9382));

        } catch (OutOfRangeException | DuplicateException o) {
            fail("No exception was to be thrown");
        }
    }

    @Test
    public void testSearchByCourseCodeThatDoesNotExist() {
        try {
            Course course1 = new Course(1883, "CHEM 301", 5029, "Chemistry Building", 3312, "Ms. Thomas", 3);
            database.addCourse(course1);
            assertNull( database.searchByCourseCode(9382));

        } catch (OutOfRangeException | DuplicateException o) {
            fail("No exception was to be thrown");
        }
    }

    private CourseTableModel createModel() {
        CourseTableModel model;
        CourseDatabase modelDatabase = new CourseDatabase();
        try {
            for (int i = 0; i < 3; i++) {
                modelDatabase.addCourse(new Course(2001 + i, "SOIL 12" + i, 8092 + i, "Forestry Building", 8994, "Mr. Brown", 3));
            }
        } catch (OutOfRangeException | DuplicateException e) {
            fail("No exception was to be thrown");
        }
        model = new CourseTableModel(modelDatabase);
        return model;
    }
}
