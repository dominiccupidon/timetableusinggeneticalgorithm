package model;

import model.scheduler.Chromosome;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import validation.DuplicateException;
import validation.OutOfRangeException;

import static org.junit.jupiter.api.Assertions.*;

public class TimeTableModelTest {
    private CourseDatabase courseDatabase;
    private TimeTableModel model;
    private  String [] columnHeaders = { "Time", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday"};

    @BeforeEach
    public void runBefore() {
        courseDatabase = new CourseDatabase();
        createTestDatabase();
        model = new TimeTableModel(new Chromosome(courseDatabase));
    }

    @Test
    public void testConstructorWithData() {
        assertEquals(columnHeaders[0], model.getColumnName(0));
        assertEquals(columnHeaders[1], model.getColumnName(1));
        assertEquals(columnHeaders[2], model.getColumnName(2));
    }

    @Test
    public void testConstructorWithOutData() {
        model = new TimeTableModel();
        assertEquals(" ", model.getValueAt(4,4));
        assertEquals(columnHeaders[0], model.getColumnName(0));
        assertEquals(columnHeaders[1], model.getColumnName(1));
        assertEquals(columnHeaders[2], model.getColumnName(2));
    }

    @Test
    public void testSetCellEditable() {
        assertFalse(model.isCellEditable(0, 1));
        assertFalse(model.isCellEditable(1, 0));
        assertFalse(model.isCellEditable(3, 4));
        model.setCellEditable(true);
        assertTrue(model.isCellEditable(4, 2));
        assertFalse(model.isCellEditable(0, 1));
        assertFalse(model.isCellEditable(1, 0));
    }

    private void createTestDatabase() {
        for (int i = 0; i < 5; i++) {
            try {
                Course course = new Course(1000 + i, "MATH " + i, 4657 + i, "Math Building", 1983, "Mr. Richards", 3);
                courseDatabase.addCourse(course);
            } catch (OutOfRangeException | DuplicateException e) {
                fail("No exception was to be thrown");
            }
        }
    }
}
