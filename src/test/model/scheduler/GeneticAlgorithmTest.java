package model.scheduler;

import model.Course;
import model.CourseDatabase;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import validation.DuplicateException;
import validation.OutOfRangeException;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.fail;

public class GeneticAlgorithmTest {
    private CourseDatabase database;
    private GeneticAlgorithm algorithm;

    @BeforeEach
    public void runBefore() {
        database = new CourseDatabase();
        createTestDatabase();
        algorithm = new GeneticAlgorithm(database);
    }

    @Test
    public void testGetSolution() {
        assertNotNull(algorithm.getSolution());
    }

    private void createTestDatabase() {
        for (int i = 0; i < 5; i++) {
            try {
                Course course = new Course(1000 + i, "MATH " + i, 4657 + i, "Math Building", 1983, "Mr. Richards", 3);
                database.addCourse(course);
            } catch (OutOfRangeException | DuplicateException e) {
                fail("No exception was to be thrown");
            }
        }
    }
}
