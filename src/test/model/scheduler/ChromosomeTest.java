package model.scheduler;

import model.Course;
import model.CourseDatabase;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import validation.DuplicateException;
import validation.OutOfRangeException;

import static org.junit.jupiter.api.Assertions.*;

//TODO Ask on Piazza if I have to test mutate()
public class ChromosomeTest {
    Chromosome chromosome;
    CourseDatabase courseDatabase;

    @BeforeEach
    public void runBefore() {
        courseDatabase = new CourseDatabase();
        createTestDatabase();
        chromosome = new Chromosome(courseDatabase);
    }

    @Test
    public void testConstructorUsingCourseDatabase() {
        assertEquals(0, chromosome.getFitness());
        assertNotNull(chromosome.getChromosome(0, Chromosome.SLOTS));
    }

    @Test
    public void testConstructorUsingBitSet() {
        Chromosome subChromosome = new Chromosome(chromosome.getChromosome(0, Chromosome.SLOTS), courseDatabase);
        assertEquals(0, subChromosome.getFitness());
        assertNotNull(chromosome.getChromosome(0, 5));
    }

    @Test
    public void testEvalFitness() {
        chromosome.evalFitness();
        assertNotEquals(0, chromosome.getFitness());
    }

    private void createTestDatabase() {
        for (int i = 0; i < 5; i++) {
            try {
                Course course = new Course(1000 + i, "MATH " + i, 4657 + i, "Math Building", 1983, "Mr. Richards", 3);
                courseDatabase.addCourse(course);
            } catch (OutOfRangeException | DuplicateException e) {
                fail("No exception was to be thrown");
            }
        }
    }
}
