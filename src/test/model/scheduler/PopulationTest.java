package model.scheduler;

import model.Course;
import model.CourseDatabase;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import validation.DuplicateException;
import validation.OutOfRangeException;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.fail;

public class PopulationTest {
    private CourseDatabase database;
    private Population population;

    @BeforeEach
    public void runBefore() {
        database = new CourseDatabase();
        createTestDatabase();
        population = new Population(database);
    }

    @Test
    public void testConstructor1() {
        assertNotNull(population.selectChromosomes());
        assertNotNull(population.bestChromosome());
    }

    @Test
    public void testConstructor2() {
       Population newPopulation = new Population(population.selectChromosomes());
       assertNotNull(newPopulation.selectChromosomes());
       assertNotNull(newPopulation.bestChromosome());
    }

    private void createTestDatabase() {
        for (int i = 0; i < 5; i++) {
            try {
                Course course = new Course(1000 + i, "MATH " + i, 4657 + i, "Math Building", 1983, "Mr. Richards", 3);
                database.addCourse(course);
            } catch (OutOfRangeException | DuplicateException e) {
                fail("No exception was to be thrown");
            }
        }
    }
}
