package model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import validation.OutOfRangeException;

import static org.junit.jupiter.api.Assertions.*;

class CourseTest {
    private Course course;

    @BeforeEach
    public void runBefore(){
        try {
            course = new Course(1011, "MATH 101", 4655, "Math Building",
                    8934, "Mr. Richards", 3);
        } catch (OutOfRangeException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testConstructor() {
        assertEquals(1011, course.getCourseCode());
        assertEquals("MATH 101", course.getCourseName());
        assertEquals(4655, course.getRoomCode());
        assertEquals("Math Building", course.getRoomName());
        assertEquals(8934, course.getTeacherCode());
        assertEquals("Mr. Richards", course.getTeacherName());
        assertEquals(3, course.getSessions());
    }

    @Test
    public void testSetCourseCodeProperCodeWithinRange() {
        int code = 4567;
        try {
            course.setCourseCode(code);
            assertEquals(code, course.getCourseCode());
        } catch (OutOfRangeException o) {
            fail("No exception was to be thrown");
        }
    }

    @Test
    public void testSetCourseCodeProperCodeLowerBoundary() {
        int code = Course.FIRST_CODE;
        try {
            course.setCourseCode(code);
            assertEquals(code, course.getCourseCode());
        } catch (OutOfRangeException o) {
            fail("No exception was to be thrown");
        }
    }

    @Test
    public void testSetCourseCodeProperCodeUpperBoundary() {
        int code = Course.LAST_CODE;
        try {
            course.setCourseCode(code);
            assertEquals(code, course.getCourseCode());
        } catch (OutOfRangeException o) {
            fail("No exception was to be thrown");
        }
    }

    @Test
    public void testSetCourseCodeImproperCode() {
        int code = 450;
        try {
            course.setCourseCode(code);
            assertEquals(code, course.getCourseCode());
            fail("An exception was supposed to be thrown and caught");
        } catch (OutOfRangeException o) {
            o.printStackTrace();
        }
        code = 11450;
        try {
            course.setCourseCode(code);
            assertEquals(code, course.getCourseCode());
            fail("An exception was supposed to be thrown and caught");
        } catch (OutOfRangeException o) {
            o.printStackTrace();
        }
    }

    @Test
    public void testSetCourseName() {
        String name = "BIOL 111";
        course.setCourseName(name);
        assertEquals(name, course.getCourseName());
    }

    @Test
    public void testSetRoomCodeProperCodeWithinRange() {
        int code = 9032;
        try {
            course.setRoomCode(code);
            assertEquals(code, course.getRoomCode());
        } catch (OutOfRangeException o) {
            fail("No exception was to be thrown");
        }
    }

    @Test
    public void testSetRoomCodeProperCodeLowerBoundary() {
        int code = Course.FIRST_CODE;
        try {
            course.setRoomCode(code);
            assertEquals(code, course.getRoomCode());
        } catch (OutOfRangeException o) {
            fail("No exception was to be thrown");
        }
    }

    @Test
    public void testSetRoomCodeProperCodeUpperBoundary() {
        int code = Course.LAST_CODE;
        try {
            course.setRoomCode(code);
            assertEquals(code, course.getRoomCode());
        } catch (OutOfRangeException o) {
            fail("No exception was to be thrown");
        }
    }

    @Test
    public void testSetRoomCodeImproperCode() {
        int code = 932;
        try {
            course.setRoomCode(code);
            assertEquals(code, course.getRoomCode());
            fail("An exception was to be caught");
        } catch (OutOfRangeException o) {
            o.printStackTrace();
        }
        code = 11342;
        try {
            course.setRoomCode(code);
            assertEquals(code, course.getRoomCode());
            fail("An exception was to be caught");
        } catch (OutOfRangeException o) {
            o.printStackTrace();
        }
    }

    @Test
    public void testSetRoomName() {
        String name = "Biological Sciences";
        course.setRoomName(name);
        assertEquals(name, course.getRoomName());
    }

    @Test
    public void testSetTeacherCodeProperCodeWithinRange() {
        int code = 6453;
        try {
            course.setTeacherCode(code);
            assertEquals(code, course.getTeacherCode());
        } catch (OutOfRangeException o) {
            fail("No exception was to be thrown");
        }
    }

    @Test
    public void testSetTeacherCodeProperCodeLowerBoundary() {
        int code = Course.FIRST_CODE;
        try {
            course.setTeacherCode(code);
            assertEquals(code, course.getTeacherCode());
        } catch (OutOfRangeException o) {
            fail("No exception was to be thrown");
        }
    }

    @Test
    public void testSetTeacherCodeProperCodeUpperBoundary() {
        int code = Course.LAST_CODE;
        try {
            course.setTeacherCode(code);
            assertEquals(code, course.getTeacherCode());
        } catch (OutOfRangeException o) {
            fail("No exception was to be thrown");
        }
    }

    @Test
    public void testSetTeacherCodeImproperCode() {
        int code = 172;
        try {
            course.setTeacherCode(code);
            assertEquals(code, course.getTeacherCode());
            fail("An exception was to be caught");
        } catch (OutOfRangeException o) {
            o.printStackTrace();
        }
        code = 11752;
        try {
            course.setTeacherCode(code);
            assertEquals(code, course.getTeacherCode());
            fail("An exception was to be caught");
        } catch (OutOfRangeException o) {
            o.printStackTrace();
        }
    }

    @Test
    public void testSetTeacherName() {
        String name = "Ms. Adamson";
        course.setTeacherName(name);
        assertEquals(name, course.getTeacherName());
    }

    @Test
    public void testSetSessionsWithinRange() {
        int sessions = 3;
        try {
            course.setSessions(sessions);
            assertEquals(sessions, course.getSessions());
        } catch (OutOfRangeException o) {
            fail("No exception was to be thrown");
        }
    }

    @Test
    public void testSetSessionsMinAmount() {
        int sessions = Course.MIN_SESSIONS_PER_WEEK;
        try {
            course.setSessions(sessions);
            assertEquals(sessions, course.getSessions());
        } catch (OutOfRangeException o) {
            fail("No exception was to be thrown");
        }
    }

    @Test
    public void testSetSessionsMaxAmount() {
        int sessions = Course.MAX_SESSIONS_PER_WEEK;
        try {
            course.setSessions(sessions);
            assertEquals(sessions, course.getSessions());
        } catch (OutOfRangeException o) {
            fail("No exception was to be thrown");
        }
    }

    @Test
    public void testSetSessionsImproperNumber() {
        int sessions = 0;
        try {
            course.setSessions(sessions);
            assertEquals(sessions, course.getSessions());
            fail("An exception was to be caught");
        } catch (OutOfRangeException o) {
            o.printStackTrace();
        }
        sessions = 5;
        try {
            course.setSessions(sessions);
            assertEquals(sessions, course.getSessions());
            fail("An exception was to be caught");
        } catch (OutOfRangeException o) {
            o.printStackTrace();
        }
    }
}