# Timetable Creator

## About
The following Java project is a timetable creator that employs a form of genetic algorithm which aims to determine the 
optimal schedule based on the course information given.

## Disclaimer
This project was a requirement of a Computer Science course aimed at introducing students
to the principles of software engineering used in object-oriented programming.

The headings used are simply a part of the grading scheme; especially the last section, which required
students to explain how we made one of our classes robust, and how we implemented one of the design patterns
learnt in class. 

I have kept these headings here as I believe that they encapsulate the essence of the project 
along with my thinking at the time. However, I have updated the language used in various parts of the document.

**As of writing this message, I am unsure if there will be any updates in the near future.**

## Description
The following application intends to be timetable manager which will help
schools manage their courses. Timetable Officers will input the following information
for each course:

- Course Name and Course Code

- Room Name and Room Code

- Number of Sessions per Week

- Professor and Professor Code

This information will be stored within a database that can be edited and searched
through as the user desires. The programme will then be able to automatically suggest 
a schedule for the student. This timetable will be saved, so the officer can access it
for future use.

My interest in this project is due to the fact that I have attempted this project before
using the C language, but was never able to fully accomplish what I wanted. Therefore, I
wanted to try to recreate it a new way using an object-oriented system.

## User Stories
As a user, I want to be able to add the information of one course to a database.

As a user, I want to be able to run search queries on the database:

As a user, I want to be able to edit the information of a course

As a user, I want to be able to delete a course from the database

As a user, I want to be able to see a list of all the courses in the database

As a user, I want the data that I enter to be validated for errors in user input

As a user, I want to have the option to save the database on a file for later use when exiting the programme

As a user, I want to have the option to load from a save file upon starting the programme

As a user, I want to be able to create a timetable using the database of courses

## How to use the Interface
When initially running the program, a dialog box is going to appear prompting you to either enter course data manually
or read from a file. 

If you choose to enter this data manually, you will be taking to a screen that allows you to do so. At the bottom 
of this screen there is two buttons, one adds the course to the database, and the other completes the 
creation of the database. You can only add courses to the database if all the data fields for the course have 
been filled out. Note that the IDs for courses must be unique to that course.

Once data entry is complete, you will be taken to the main screen. The main screen is divided into two tabs: one
deals with solely with the Course database and another for the Time Table.
 
On the first screen, a table of the courses in the database is displayed. Below the table is a search bar which filters 
the course based on the text entered.

There are three options below the search option:
- Edit: Allows you to freely edit the course information within the cells of the table
- Delete: Removes a single row/course that is selected from the table/database
- Add: Takes you to the data entry screen to manually add as many courses as desired.

For the first two options press the "Save Changes" button to commit the changes to the database.

The second screen will display an empty table that shows the days of the week, and the times which class can be held. 
Below this table is a button that says "Create Time Table". Each time the button is pressed a new schedule will be 
generated.

Closing the window automatically saves the database to a file, unless there is and error within the database;
if so the user is alerted that the database will not be saved, and the programme closes.

## Re: Project Phases
### Phase 4: Task 2

I opted to design a robust class using the Course and CourseDatabase classes. Within the Course class, the ID codes for 
the course, room and teacher along with the number of sessions per week must be within a certain range. If not a special
OutOfRangeException will be thrown. Within the CourseDatabase class, a check is done to ensure that course ID codes are 
unique, if this check fails then a special DuplicateException Code will be thrown.

### Phase 4: Task 3

Before, if I wanted to make a change to every course in the database, I would have to get the length of the database and
use a special method to access the individual Course. Therefore, I decided to make the CourseDatabase class implement 
Iterable to prevent significantly decrease the instances in which another class would know how the courses are stored.

